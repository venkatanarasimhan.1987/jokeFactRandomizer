package com.example.venkata.assignment1randomizer;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

/**
 * Created by venkata_narasimhan on 2/22/2017.
 */

public class RandomPageActivity extends AppCompatActivity {
    TextView tv_DataDisplay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datatextview);

        Intent newIntent = getIntent();
        String joke = newIntent.getStringExtra("jokeValue");
        tv_DataDisplay = (TextView) findViewById(R.id.tv_dataValue);
        tv_DataDisplay.setText(joke);
    }
}
