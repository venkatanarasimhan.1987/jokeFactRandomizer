package com.example.venkata.assignment1randomizer;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class HomePageActivity extends AppCompatActivity {

    //For saved instance state
    public static final String TAG = "RANDOMIZER";
    public static int counter = 0;

    public Spinner choicespinner;
    public static String spinnerValue;

    public ImageButton ib_dinosaurs;
    public ImageButton ib_mammals;
    public ImageButton ib_humans;
    public ImageButton ib_birds;

    //Random button
    public Button btn_Randomizer;

    //Last Joke Button
    public Button btn_lastJokeFact;

    //Shared preferences
    String[] mJokesFactsArray;
    TextView tv_DataDisplay;
    ArrayList mJokesFactsArrayMain = new ArrayList();
    public Button btn_aboutMe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);

        //shared preferences
        SharedPreferences sPrefs = getPreferences(MODE_PRIVATE);
        tv_DataDisplay = (TextView) findViewById(R.id.tv_body);
        String tView1 = sPrefs.getString("TextView1"," ");
        tv_DataDisplay.setText(tView1);
        tv_DataDisplay.setVisibility(View.INVISIBLE);
        //Spinner content value
        choicespinner = (Spinner) findViewById(R.id.choice_spinner);
        ArrayAdapter<CharSequence> adapter =
                ArrayAdapter.createFromResource(this,
                        R.array.intents, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        choicespinner.setAdapter(adapter);
        choicespinner.setPrompt("Pick a CATEGORY");


        if(counter == 0){
            addListenerOnButton();
        }else {
            addListenerOnButton();
            updateLayout(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        updateLayout(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE);
    }



    private void updateLayout(boolean isLandscape) {
        if (isLandscape) {
            tv_DataDisplay.setVisibility(View.VISIBLE);
        } else {
            tv_DataDisplay.setVisibility(View.VISIBLE);
        }
    }

    //Add button listener
    public void addListenerOnButton() {
        final Context context = this;

        //Setting the image button
        ib_dinosaurs = (ImageButton) findViewById(R.id.ibtn_dinosaur);
        ib_mammals = (ImageButton) findViewById(R.id.ibtn_mammals);
        ib_humans = (ImageButton) findViewById(R.id.ibtn_humans);
        ib_birds = (ImageButton) findViewById(R.id.ibtn_birds);

        //Setting the random button
        btn_Randomizer = (Button) findViewById(R.id.btn_random);

        //Setting the last joke fact button
        btn_lastJokeFact = (Button) findViewById(R.id.btn_lastDataReturn);

        //Setting the about me button
        btn_aboutMe = (Button) findViewById(R.id.btn_aboutMe);

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.equals(ib_dinosaurs)) {
                    //Getting the spinners value
                    if(choicespinner.getSelectedItem().toString().equals("Give me a Joke")){
                        spinnerValue = "Give me a Joke";
                        mJokesFactsArray =   getResources().getStringArray(R.array.jokesfactsdinosaurs);
                        Intent intent = new Intent(context,DinoDataActivity.class);
                        intent.putExtra("jokeValue",updateTextView());
                        startActivity(intent);
                        counter++;
                        updateCounter();
                    }else {
                        spinnerValue = "Give me a Fact";
                        mJokesFactsArray =   getResources().getStringArray(R.array.jokesfactsdinosaurs);
                        Intent intent = new Intent(context,DinoDataActivity.class);
                        intent.putExtra("jokeValue",updateTextView());
                        startActivity(intent);
                        counter++;
                        updateCounter();
                    }
                }else if (v.equals(ib_mammals)){
                    if(choicespinner.getSelectedItem().toString().equals("Give me a Joke")){
                        spinnerValue = "Give me a Joke";
                        mJokesFactsArray =   getResources().getStringArray(R.array.jokesfactsmammals);
                        Intent intent = new Intent(context,MammalsDataActivity.class);
                        intent.putExtra("jokeValue",updateTextView());
                        startActivity(intent);
                        counter++;
                        updateCounter();
                    }else {
                        spinnerValue = "Give me a Fact";
                        mJokesFactsArray =   getResources().getStringArray(R.array.jokesfactsmammals);
                        Intent intent = new Intent(context,MammalsDataActivity.class);
                        intent.putExtra("jokeValue",updateTextView());
                        startActivity(intent);
                        counter++;
                        updateCounter();
                    }
                }else if (v.equals(ib_birds)){
                    if(choicespinner.getSelectedItem().toString().equals("Give me a Joke")){
                        spinnerValue = "Give me a Joke";
                        mJokesFactsArray =   getResources().getStringArray(R.array.jokesfactsbirds);
                        Intent intent = new Intent(context,BirdDataActivity.class);
                        intent.putExtra("jokeValue",updateTextView());
                        startActivity(intent);
                        counter++;
                        updateCounter();
                    }else {
                        spinnerValue = "Give me a Fact";
                        mJokesFactsArray =   getResources().getStringArray(R.array.jokesfactsbirds);
                        Intent intent = new Intent(context,BirdDataActivity.class);
                        intent.putExtra("jokeValue",updateTextView());
                        startActivity(intent);
                        counter++;
                        updateCounter();
                    }
                }else if (v.equals(ib_humans)){
                    if(choicespinner.getSelectedItem().toString().equals("Give me a Joke")){
                        spinnerValue = "Give me a Joke";
                        mJokesFactsArray =   getResources().getStringArray(R.array.jokesfactshumans);
                        Intent intent = new Intent(context,HumanDataActivity.class);
                        intent.putExtra("jokeValue",updateTextView());
                        startActivity(intent);
                        counter++;
                        updateCounter();
                    }else {
                        spinnerValue = "Give me a Fact";
                        mJokesFactsArray =   getResources().getStringArray(R.array.jokesfactshumans);
                        Intent intent = new Intent(context,HumanDataActivity.class);
                        intent.putExtra("jokeValue",updateTextView());
                        startActivity(intent);
                        counter++;
                        updateCounter();
                    }
                }else if(v.equals(btn_lastJokeFact)){
                    tv_DataDisplay.setVisibility(View.INVISIBLE);
                    if(counter == 0 && tv_DataDisplay.getText().toString().equals(" ")){
                        Toast.makeText(getApplicationContext(),"NOTHING SAVED", Toast.LENGTH_LONG).show();
                    }else {
                        Intent intent = new Intent(context,LastDataActivity.class);
                        intent.putExtra("jokeValue",updateTextViewForLastJokeFact());
                        startActivity(intent);
                        updateCounter();
                    }
                }else if(v.equals(btn_aboutMe)){
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);

                    builder.setMessage(R.string.dialog_message)
                            .setTitle(R.string.dialog_title);

                    AlertDialog dialog = builder.create();
                    dialog.show();
                }else{
                    Collections.addAll(mJokesFactsArrayMain, getResources().getStringArray(R.array.jokesfactsbirds));
                    Collections.addAll(mJokesFactsArrayMain, getResources().getStringArray(R.array.jokesfactsmammals));
                    Collections.addAll(mJokesFactsArrayMain, getResources().getStringArray(R.array.jokesfactsdinosaurs));
                    Collections.addAll(mJokesFactsArrayMain, getResources().getStringArray(R.array.jokesfactshumans));

                    Intent intent = new Intent(context,RandomPageActivity.class);
                    intent.putExtra("jokeValue",updateTextViewForRandomJokeFact());
                    startActivity(intent);
                    counter++;
                    updateCounter();
                }
            }
        };

        //Image button onclick listener

        ib_dinosaurs.setOnClickListener(listener);
        ib_birds.setOnClickListener(listener);
        ib_mammals.setOnClickListener(listener);
        ib_humans.setOnClickListener(listener);
        //Randomizer button listener.
        btn_Randomizer.setOnClickListener(listener);
        //Last joke fact listener
        btn_lastJokeFact.setOnClickListener(listener);
        //about me listener
        btn_aboutMe.setOnClickListener(listener);
    }

    private String updateTextView() {
        tv_DataDisplay = (TextView)findViewById(R.id.tv_body);
        tv_DataDisplay.setVisibility(View.INVISIBLE);
        Random random = new Random();

        //Choose between Jokes
        if(HomePageActivity.spinnerValue == "Give me a Joke"){
            int maxIndex = mJokesFactsArray.length - 5;
            int generatedIndex = random.nextInt(maxIndex);
            tv_DataDisplay.setText("Enjoy your joke" +"\n"+"\n"+mJokesFactsArray[generatedIndex]);

            SharedPreferences spref = getPreferences(MODE_PRIVATE);
            SharedPreferences.Editor editor = spref.edit();

            String tv1 = tv_DataDisplay.getText().toString();
            editor.putString("TextView1",tv1);
            editor.commit();
            return tv_DataDisplay.getText().toString();
        }
        //Choose between Facts
        else{
            int maxIndex = mJokesFactsArray.length;
            int generatedIndex = random.nextInt(maxIndex);
            if(generatedIndex >= 5) {
                tv_DataDisplay.setText("Enjoy your fact" +"\n"+"\n"+mJokesFactsArray[generatedIndex]);
                SharedPreferences spref = getPreferences(MODE_PRIVATE);
                SharedPreferences.Editor editor = spref.edit();

                String tv1 = tv_DataDisplay.getText().toString();
                editor.putString("TextView1",tv1);
                editor.commit();
                return tv_DataDisplay.getText().toString();
            }else{
                tv_DataDisplay.setText("Enjoy your fact" +"\n"+"\n"+mJokesFactsArray[generatedIndex + 5]);
                SharedPreferences spref = getPreferences(MODE_PRIVATE);
                SharedPreferences.Editor editor = spref.edit();

                String tv1 = tv_DataDisplay.getText().toString();
                editor.putString("TextView1",tv1);
                editor.commit();
                return tv_DataDisplay.getText().toString();
            }
        }
    }

    private String updateTextViewForRandomJokeFact() {
        tv_DataDisplay = (TextView)findViewById(R.id.tv_body);
        tv_DataDisplay.setVisibility(View.INVISIBLE);

        Random random = new Random();
        int maxIndex = mJokesFactsArrayMain.size();
        int generatedIndex = random.nextInt(maxIndex);
        tv_DataDisplay.setText("\n Your Randomizer gives you:"+"\n"+
                "\n" + mJokesFactsArrayMain.get(generatedIndex));

        return tv_DataDisplay.getText().toString();
    }

    private String updateTextViewForLastJokeFact() {
        return tv_DataDisplay.getText().toString();
    }

    ////////////////////////////////////////////////////////////////////////////
    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putInt("counter", counter);
    }

    private void updateCounter() {
        TextView textView = (TextView) findViewById(R.id.tv_counterArea);
        Log.d(TAG, "onUpdateCounter() ");
        textView.setText(" "+counter);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        counter = savedInstanceState.getInt("counter");
        tv_DataDisplay.setVisibility(View.INVISIBLE);
        updateCounter();
    }
    ////////////////////////////////////////////////////////////////////////////

    protected void onStop(){
        super.onStop();
        SharedPreferences spref = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = spref.edit();

        String tv1 = tv_DataDisplay.getText().toString();
        editor.putString("TextView1",tv1);
        editor.commit();
    }
}
